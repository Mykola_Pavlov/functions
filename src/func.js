/**
 * The function gets two infinite numbers as strings and
 * return the result of these two numbers sum as string.
 * @param {string} str1 - A string of number.
 * @param {string} str2 - A string of number.
 * @return {string} The sum of the strings of the input parameters or false if
 * input parameters are incorected.
 */
const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' || typeof str2 !== 'string' || isNaN(Number(str1)) || isNaN(Number(str2))) {
    return false;
  }
  const str1InArr = str1.length === 0 ? [0] : str1.split('');
  const str2InArr = str2.length === 0 ? [0] : str2.split('');

  if (str1InArr.length > str2InArr.length) {
    do {
      str2InArr.unshift(0);
    }
    while (str2InArr.length !== str1InArr.length)
  }
  else if (str2InArr.length > str1InArr.length) {
    do {
      str1InArr.unshift(0);
    }
    while (str1InArr.length !== str2InArr.length)
  }

  let result = (new Array(str1InArr.length + 1)).fill(0);

  for (let i = str1InArr.length - 1; i >= 0; i--) {
    const localRes = `0${parseInt(str1InArr[i]) + parseInt(str2InArr[i])}`;
    result[i + 1] += parseInt(localRes.slice(-1));
    result[i] += parseInt(localRes[localRes.length - 2]);
  }
  result = parseInt(result.join('')).toString();
  return result;
};

/**
 * The function returns quantity of posts with author from argument of function and the quantity of
 * all comments with the same author.
 * @param {Array} listOfPosts - An array of objects.
 * @param {string} authorName - A name of author.
 * @return {string} Post:number of post,comments:number of comments.
 */
const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let post = 0;
  let comments = 0;
  for (let i = 0; i < listOfPosts.length; i++) {
    if (listOfPosts[i].author === authorName) {
      post++;
    }
    if (listOfPosts[i].hasOwnProperty('comments')) {
      for (let j = 0; j < listOfPosts[i].comments.length; j++) {
        if (listOfPosts[i].comments[j].author === authorName) {
          comments++;
        }
      }
    }
  }
  return `Post:${post},comments:${comments}`;
};

/**
 * The function determines whether the clerk can sell tickets to everyone in the queue
 * if he initially had no money.
 * @param {Array} people - an array of banknotes of a queue of people (can be array of string).
 * @return {string} 'YES', if he can sell, 'NO' - if he can't.
 */
const tickets = (people) => {
  const paymentArr = [];

  for (let i = 0; i < people.length; i++) {
    if (people[i] % 25 !== 0) {
      return 'NO';
    }
    const payment = people[i] - 25;
    if (payment === 75) {
      const firstPaymentInd = paymentArr.indexOf(25);
      const secondPaymentInd = paymentArr.indexOf(50);
      if (firstPaymentInd !== -1 && secondPaymentInd !== -1) {
        paymentArr.splice(firstPaymentInd, 1);
        paymentArr.splice(secondPaymentInd, 1);
        paymentArr.push(100);
        continue;
      }
      return 'NO';
    }
    if (payment === 25) {
      const firstPaymentInd = paymentArr.indexOf(25);
      if (firstPaymentInd !== -1) {
        paymentArr.splice(firstPaymentInd, 1);
        paymentArr.push(50);
        continue;
      }
      return 'NO';
    }
    paymentArr.push(25);
  }
  return 'YES';
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
